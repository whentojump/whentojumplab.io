---
title: "Projects"
layout: "single" # otherwise pages at this level will default to "list"
ShowToc: true
ContentBeforeToc: |
  <div style="color: var(--warning)">
    <p>Recently this page has been less actively maintained. But let me try to keep it sync'ed :)</p>
    <p style="text-align: right">Wentao<br>May 2024</p>
  </div>

  Selected research projects.

  You can find more of them [here](unsorted).
---

## On adequacy of Linux kernel tests & reliability of coverage measurement tools

Advisors: [Tianyin Xu](https://tianyin.github.io/), [Darko Marinov](https://mir.cs.illinois.edu/marinov/)

[Publications]
[[Talks](https://elisa.tech/blog/2024/05/28/making-linux-fly-towards-certified-linux-kernel/)]
[[Software](https://github.com/xlab-uiuc/linux-mcdc)]

Contributions to open-source projects:

- LLVM: https://github.com/llvm/llvm-project/commits/?author=whentojump
- Linux kernel
- ...

This work is a collaboration with [Steve VanderLeest](https://www.linkedin.com/in/vanderleest) and many other fantastic people from The Boeing Company.

<!--
## Implications of silent data corruption to distributed systems

Collaborators: [Zijie Zhao](https://zzjas.com/), [Ramnatthan Alagappan](https://ramalagappan.github.io/), [Aishwarya Ganesan](https://aishwaryaganesan.github.io/), [Tianyin Xu](https://tianyin.github.io/)
-->

## On reliability of cloud-native systems

[[Publications](https://dl.acm.org/doi/10.1145/3600006.3613161)]
[[Software](https://github.com/xlab-uiuc/acto)]

Advisor: [Tianyin Xu](https://tianyin.github.io/)

During this project, I learned a lot from and had an incomparable experience with
my amazing mentors [Tyler Gu](https://tylergu.com/)
and [Xudong Sun](https://marshtompsxd.github.io/). Thanks!

<p style="color: var(--warning)">
  ---8<------8<--- Older stuffs below. May be moved around. ---8<------8<---
</p>

## Cycle-accurate record and replay of cloud FPGAs

Advisor: [Baris Kasikci](https://web.eecs.umich.edu/~barisk/)

[Publications]
[[Software](https://github.com/efeslab/aws-fpga-carr)]<sup>[<em>open soon</em>]</sup>

<!--
<div class="spoiled spoiler-blurred">
  <p style="color: var(--warning)">
    COMING SOON: Some top secrets are encoded in <a href="https://en.wikipedia.org/wiki/Lorem_ipsum">alien language</a> now 🤔
  </p>
  <p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis felis risus. Vestibulum ac diam sit amet sem efficitur imperdiet semper eu felis. Integer arcu sapien, posuere non massa maximus, iaculis auctor ligula. Nunc blandit dictum lorem, aliquet dignissim arcu facilisis nec. Praesent congue cursus nunc, a sagittis felis aliquam ac. Vivamus finibus turpis massa, vel sollicitudin augue facilisis nec. Duis quis urna vitae risus venenatis mollis. Praesent ultrices leo sed arcu faucibus condimentum. Mauris ullamcorper id orci accumsan pulvinar. Maecenas congue nulla sollicitudin, maximus elit sed, accumsan nunc. Sed eget ligula blandit, placerat erat sed, fermentum diam.
  </p>
</div>
-->

During this project, I was fortunate to be mentored by [Jiacheng Ma](https://jcma.me/)
and [Gefei Zuo](https://web.eecs.umich.edu/~gefeizuo/), and to work with Yin Yuan.

![carr.png](carr.png)

<!-- **Figure 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis felis risus. Vestibulum ac diam sit amet sem efficitur imperdiet semper eu felis.** -->

## Safe and expressive kernel extensions in Rust

Advisor: [Tianyin Xu](https://tianyin.github.io/)

[[Publications](https://dl.acm.org/doi/10.1145/3593856.3595892)]
[[Software](https://github.com/rosalab/rex-kernel)]<sup>[<em>open soon</em>]</sup>

[Berkeley packet filter (BPF)](https://lwn.net/Articles/740157/) subsystem
offers a promising way of extending the kernel in networking, observability,
security and many other use cases. However, to ensure safety, the native BPF
verifier imposes several restrictions to the properties of BPF programs (like
size and control flow), thus limiting their expressiveness. In this work, we
propose a new mechanism of kernel extension, by placing compiled Rust programs
at hook points that were conventionally for BPF programs to run, and turning to
the language, rather than the verifier, for safety guarantee.

I contributed to:

* **Kernel, runtime and compiler support**: We are [adding necessary system
calls for loading the programs](https://github.com/djwillia/linux/tree/inner_unikernels),
inserting a runtime crate to perform {helper, argument} type checks, and
[building LLVM passes within `rustc`](https://github.com/xlab-uiuc/rust/tree/inner-unikernel-dev)
to further manipulate the programs and enforce safety policy.
* **"Application" development**: We are rewriting practical BPF programs with
the new abstraction, from relatively simple ones (e.g. those shipped in the
kernel source tree, like [`trace_event`](https://elixir.bootlin.com/linux/v5.15/source/samples/bpf/trace_event_kern.c)),
to nontrivial ones (e.g. [XRP](https://www.usenix.org/conference/osdi22/presentation/zhong)).
* **Micro-benchmarking**: I am conducting [performance evaluation](https://github.com/whentojump/linux/tree/benchmark/samples/bpf/benchmark)
with respect to several factors, like tail calls and [vulnerability mitigation](https://support.google.com/faqs/answer/7625886).

During this project, I also had the honor to work with
[Dan Williams](https://people.cs.vt.edu/djwillia/)
and Jinghao Jia, among many other excellent researchers.

![iu.png](iu.png)

Figure 2<sup id="ref:iu-figure"><a href="#fn:iu-figure">1</a></sup>. BPF
programs are subject to the <a href="https://www.kernel.org/doc/html/v5.15/bpf/bpf_design_QA.html#q-what-are-the-verifier-limits">size limit</a>
imposed by the verifier (shown in <b style="color: rgb(68, 114, 196)">blue</b>).
If a program has to exceed that limit, it should first be split into smaller
_tail calls_. These tail calls then get chained together in order to execute
(shown in <b style="color: rgb(237, 125, 49)">orange</b>, each dot representing
one call). One of our goals is to let standalone Rust programs cross the same
boundary, while maintaining comparable performance.

---

<ol class="footnotes">
  <li id="fn:iu-figure">
    <p>
      Incidentally, I'm not sure whether you have noticed, the figure looks
      pretty decent either in light or dark mode. Try it out:
      <a id="extra-theme-toggle" style="cursor: pointer">toggle</a>.
      I was simply shocked by this effect! and did some serious research about
      it. The black magic turns out to be:
      <span class="spoiled spoiler-blurred">
        <em>grey will look white if on black, and will look black if on white.
        </em> I'd really like to share this <em>less academic</em>, but quite
        philosophical finding, in case you are about to search something like
        "PNG 3.0" for hours like I did.
      </span>
      <a href="#ref:iu-figure">↩︎</a>
    </p>
  </li>
</ol>

## IEEE 1451 standard development

Advisor: [Jun Wu](https://icst.sjtu.edu.cn/DirectoryDetail.aspx?id=1)

[[Publications](https://ieee-p21451-1-5.github.io/#publications)]
[[Software](https://github.com/ieee-p21451-1-5)]

IEEE 1451 is a family of standards whose goal is to promote interoperability
among various interfaces or network protocols commonly seen in Internet of
Things (IoT). I am a member of the [working group](https://standards.ieee.org/project/21451-1-5.html)
led by Prof. Jun Wu, with a focus on the network level.

My contributions include:

* **Specifications on IoT management**: My group's mission is to standardize the
way of accessing and managing IoT with [Simple Network Management Protocol (SNMP)](https://www.rfc-editor.org/info/std62).
To this end, we carefully investigated the common practices in industry,
identified the gap between current approaches and new demands of interoperable
IoT, and formulated draft specifications in terms of reference model, message
structure, management information, etc.
* **Specifications on security TEDS**: I also worked closely with [colleagues from NIST](#nist-colleagues)
on security transducer electronic datasheet (TEDS), which helps IoT applications
know about the security protocols being used (e.g. TLS for MQTT, [VACM for SNMP](https://www.rfc-editor.org/rfc/rfc3415),
etc.) and their parameters in a zero-configuration manner.
* **Hardware and software porototyping**: I developed [more than three generations](unsorted/#ieee-1451-standard-development-2018-2022)
of the hardware prototypes with heterogeneous computing devices (from ARM-based
microcontrollers, single-board computers, to x86-based industrial PCs). I also
participated in [software development](https://github.com/ieee-p21451-1-5) on
top of these platforms, implementing/<wbr>validating the specifications of the
draft standard. The prototypes were [presented at IECON'18/<wbr>19/<wbr>20/<wbr>21 and ISIE'21](https://ieee-p21451-1-5.github.io/#events).
A live demonstration has also been set up and running since 2020, connecting
multiple testbeds from other teams around the world.

<p id="nist-colleagues" class="fancy-jump-target">
  During this project, I also worked closely with <a href="https://ieee-ims.org/contact/kang-lee">Kang Lee</a>
  and <a href="https://www.nist.gov/people/eugene-song">Eugene Song</a>
  (check this nice <a href="https://whentojump.gitlab.io/a/eugene-kang-wentao-lisbon-2019.jpg">photo</a><sup id="ref:photo-credit"><a href="#fn:photo-credit">1</a></sup>
  of us!) among many other respected industry experts.
</p>

![ieee1451-demo-2021-3d.png](ieee1451-demo-2021-3d.png)

Figure 3<sup><a href="#fn:logo" id="ref:logo">2</a>,<a href="#fn:slogan" id="ref:slogan">3</a></sup>.
Transducer interface module (TIM) 4.0<sup id="ref:versioning"><a href="#fn:versioning">4</a></sup>,
one of many hardware prototypes I developed for this project. Notably, it has
two processing units: [a microcontroller](https://www.st.com/en/microcontrollers-microprocessors/stm32f103c8.html),
and [a SoC](https://www.raspberrypi.com/documentation/computers/processors.html#bcm2837)
shipped along with the Raspberry Pi. It thus incorporates the benefits of both:
(1) real-time capabilities of the microcontroller (2) OS, runtime and
networkability of the Raspberry Pi<sup id="ref:rpi-mcu-diff"><a href="#fn:rpi-mcu-diff">5</a></sup>.
Moreover, the two are connected through an on-board serial bus, making
inter-chip communication or even ["over-the-air" firmware upgrade](https://electronics.stackexchange.com/questions/590426/what-are-the-boot10-series-resistors-used-for-in-stm32)
possible.

---

<ol class="footnotes">
  <li id="fn:photo-credit">
    <p>
      Photo credit: Jie Ren.
      <a href="#ref:photo-credit">↩︎</a>
    </p>
  </li>

  <li id="fn:logo">
    <p>
      Processor (and logo) credits: <a href="https://www.raspberrypi.org/">Raspberry Pi Foundation</a>
      and <a href="https://www.st.com/">STMicroelectronics</a>.
      <a href="#ref:logo">↩︎</a>
    </p>
  </li>

  <li id="fn:slogan">
    <p>
      <a href="https://www.intel.com/">Intel Corporation</a> owns the copyright
      on the "<em>whatever</em> inside" slogan. Inside, though, there are no
      actual Intel products.
      <a href="#ref:slogan">↩︎</a>
    </p>
  </li>

  <li id="fn:versioning">
    <p>
      Indeed, we never figured out a serious versioning scheme. So this is more
      like a parody of the buzzword "industry 4.0" than any meaningful release
      code, although I <em>did</em> develop and produce roughly that number of
      generations (i.e. in SemVer jargon, <a href="https://semver.org">major
      versions</a>) over the years.
      <a href="#ref:versioning">↩︎</a>
    </p>
  </li>

  <li id="fn:rpi-mcu-diff">
    <p>
      This <a href="https://sourceforge.net/p/raspberry-gpio-python/code/ci/0.7.0/tree/README.txt#l3">README</a>
      for <code>RPi.GPIO</code> project gives a very elegant summary of the
      technical differences. I like it! and cite this text over and over :)
      <a href="#ref:rpi-mcu-diff">↩︎</a>
    </p>
  </li>
</ol>

<!--
  Always put at the bottom to make sure other elements are properly loaded
  before executing any script.
-->

<!-- Blur spoiler (script) -->

<script>
nodeList = document.querySelectorAll(".spoiled");
nodeList.forEach( n => {
    n.addEventListener("click", function () {
        n.classList.remove("spoiler-blurred", "spoiled");
    });
})
</script>

<!-- Add an extra theme-toggle button -->

<script>
document.getElementById("extra-theme-toggle").addEventListener("click", () => {
    if (document.body.className.includes("dark")) {
        document.body.classList.remove('dark');
        localStorage.setItem("pref-theme", 'light');
    } else {
        document.body.classList.add('dark');
        localStorage.setItem("pref-theme", 'dark');
    }
})
</script>

<!-- Blur spoiler (style) -->

<style>
.spoiler-blurred {
    cursor: pointer;
    user-select: none;
    -webkit-user-select: none;
    filter: blur(0.5em);
}
.spoiler-blurred:hover {
    filter: blur(0.18em);
}
</style>

<!-- Make this specific in-page jump highlighted -->

<style>
.fancy-jump-target:target {
    animation: highlight 2s;
}

@keyframes highlight {
    40% {color: var(--warning);}
    60% {color: var(--warning);}
}
</style>
