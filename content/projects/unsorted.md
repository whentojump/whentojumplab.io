---
title: "Unsorted projects"
ShowToc: true
ContentBeforeToc: |
  Projects that either:

  1. are too old,
  2. are less academic and maybe [just for fun](https://www.amazon.com/Just-Fun-Story-Accidental-Revolutionary/dp/0066620732),
  3. have unfortunately failed :(
  4. or simply don't fit into any larger category.

  Take a look at a curated (and more research-oriented) list [here](../).

  <p style="color: var(--warning)">
    This page is far from complete (and so is this whole site, actually). I will
    upload some pictures and add some description at some random moments I feel
    pleased to (or have enough time to).
  </p>
---

## PCBA gallery 🎨 {#pcba-gallery}

### IEEE 1451 standard development (2018-2022)

<img alt="ieee1451-demo-2018-09.jpg" src="ieee1451-demo-2018-09.jpg" class="photo">

<p style="text-align: center">(September 2018)</p>

> **Corrigendum**
>
> The silkscreen designator "WiFi" in the lower right corner of the carrier
> board is incorrect. That connector was for an [nRF24](https://www.nordicsemi.com/products/nrf24-series)
> wireless module, which *does* operate at 2.4GHz but does *not* implement any
> IEEE 802.11 technology.

Controller: [EK-TM4C1294XL](https://www.ti.com/tool/EK-TM4C1294XL)
of Texas Instruments

<img alt="ieee1451-demo-2018-10.jpg" src="ieee1451-demo-2018-10.jpg" class="photo">

<p style="text-align: center">(October 2018)</p>

Controller: [STM32F103RCT6](https://www.st.com/en/microcontrollers-microprocessors/stm32f103rc.html)
of STMicroelectronics

<img alt="ieee1451-demo-2019.jpg" src="ieee1451-demo-2019.jpg" class="photo">

<p style="text-align: center">(2019)</p>

Ta-da! From now on, we've stepped into the era of Raspberry Pi.

<img alt="ieee1451-demo-2020.jpg" src="ieee1451-demo-2020.jpg" class="photo">

<p style="text-align: center">(2020)</p>

However, this joy didn't last long: we felt increasingly frustrated by
complaints from those Pis about poor power supply[^1], basically because most
micro-USB (for model 3B/3B+) chargers and cables are not *exceptional* enough to
feed these monsters.

[^1]: There are many indicators: (1) the output of [`vcgencmd`](https://www.raspberrypi.com/documentation/computers/os.html#get_throttled)
utility
(2) kernel log [messages](https://github.com/raspberrypi/linux/commit/74d1e007915fab590f8be9dc647b19511260210c#diff-392ab9c8a74e8f4e42b6b781dd3db37f9447930605ad44b4bcf443051016a78eR50-R53)
or (3) [a ⚡ icon](https://www.raspberrypi.com/documentation/computers/configuration.html#undervoltage-warning)
if you're connected to a display. Horrible, isn't it?

So we turned to some serious switching power supplies, with some really
*exceptional* specs like 5V 20A (!) or more. Check [this picture](https://ieee-p21451-1-5.github.io/image/demo-full.jpg)
to get a sense of how conspicuous it looks compared to the prototype itself.

Note well, this also means we are bypassing the micro-USB port at all and
directly powering Raspberry Pis through their [40-pin headers](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html#gpio-and-the-40-pin-header).
As a result, the protection[^2] around the USB circuit is bypassed as well.
Remember to replicate it on your own design right before entering the headers,
if you do care about safety, and about these adorable Pis[^3].

[^2]: Refer to the upper left part of [this schematic](https://datasheets.raspberrypi.com/rpi3/raspberry-pi-3-b-plus-reduced-schematics.pdf),
essentially, a resettable fuse ("MF-MSMF250/X" or equivalent), a [TVS](https://en.wikipedia.org/wiki/Transient-voltage-suppression_diode)
("SMBJ5.0A" or equivalent), and optionally several capacitors.
[^3]: In hindsight as of 2023, it was a clever move. Curious why? Open the
website of your local sales, and check the current stock and price.

More information about this version can be found [here](https://ieee-p21451-1-5.github.io/interop/2020/)
and [here](https://ieee-p21451-1-5.github.io/interop/2020/tutorial).

<img alt="ieee1451-demo-2021.jpg" src="ieee1451-demo-2021.jpg" class="photo">

<p style="text-align: center">(2021)</p>

This version kind of necromances many exciting bits of previous ones, like
microcontroller, battery, power monitor and so on. It also, of course, brings
even more fun.

The [Noctua fan](https://noctua.at/en/nf-a4x20-pwm) is connected to the carrier
board (via the [standard interface](https://noctua.at/pub/media/wysiwyg/Noctua_PWM_specifications_white_paper.pdf)
you may find on your PC's motherboard) and is thus fully controllable!

### Power analysis of Raspberry Pi (2020)

<img alt="rpi-power-analysis.jpg" src="rpi-power-analysis.jpg" class="photo">

[INA219](https://www.ti.com/product/INA219)

[CCS'21 paper](https://dl.acm.org/doi/abs/10.1145/3460120.3484733)

### A color identifier used in assembly line (2017)

### NUEDC and its training (2017)

Advisor: [Yan Yuan](https://ee.sjtu.edu.cn/FacultyDetail.aspx?id=140&infoid=66&flag=66)

## Connectivity of SJTU's private BitTorrent tracker 🍇 {#pt-connectivity}

By the way, here's my user bar:

<img alt="my user bar"
     style="border: 2px solid var(--border);
            border-radius: var(--radius);"
     src="https://pt.sjtu.edu.cn/mybar.php?userid=133154.png" />

## Toy CI for my research group ⚙️ {#toy-ci}

<style>
img.photo {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 70%;
}
</style>
