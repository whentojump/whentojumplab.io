---
title: "About"
layout: "single" # otherwise pages at this level will default to "list"
---

## Bio

- B.Eng. @ [Shanghai Jiao Tong University](https://en.sjtu.edu.cn/about).
- M.Eng. @ [Shanghai Jiao Tong University](https://en.sjtu.edu.cn/about).
- Ph.D. student @ [University of Illinois at Urbana-Champaign](https://illinois.edu/about/index.html).
- Research interests: computer science, systems.

## License

[WTFPL](http://www.wtfpl.net/), unless otherwise stated. Some immediate *otherwise*s:

- _except_ avatars and favicons (anime girls, mostly). They are credited to (chronological order throughout history):
    - [Yohane 😈](https://www.yohane.net/)
    - [Nijika 🌈](https://bocchi.rocks/)

## This site

<a class="gitlab-badge" href="https://gitlab.com/whentojump/whentojump.gitlab.io/-/commits/master"><img alt="last modified" src="https://img.shields.io/gitlab/last-commit/whentojump/whentojump.gitlab.io?label=modified&color=blue" /></a> <a class="gitlab-badge" href="https://gitlab.com/whentojump/whentojump.gitlab.io/-/pipelines"><img alt="pipeline status" src="https://img.shields.io/gitlab/pipeline-status/whentojump/whentojump.gitlab.io?branch=master" /></a>

- Powered by [Hugo](https://gohugo.io/) & [PaperMod](https://github.com/adityatelange/hugo-PaperMod/), with a bit [customization](https://gitlab.com/whentojump/hugo-PaperMod).
- Occasionally optimized for other bizarre screen sizes than PC or laptop, but not guaranteed.
- Hosted on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
