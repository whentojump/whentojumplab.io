---
title: "A note about ranking"
---

> [Revision history](https://gitlab.com/whentojump/whentojump.gitlab.io/-/commits/master/content/about/ranking.md) of this page

Previously I put the ranking **1/130** here and there in my profiles. But recently I find it hard to get any *official* certificate for that. So let me explain a bit (as accurately as possible).

*That ranking* is used as the criteria to tell if a senior student is eligible for "直升" or "推荐免试", which basically means admission by graduate schools without having to take the national exam ("考研"). *It is arguably the most important ranking throughout one's undergraduate study in China*.

As for my school, that ranking is calculated based on one's cumulative score of the first six semesters (because the decision of "直升" or "推荐免试" is made at the beginning of the seventh semester), *plus* a bonus score one can earn by winning a specified list of competitions (for example, in my case, National Undergraduate Electronic Design Contest).

I do have, though, some *circumstantial* evidence that can support my that ranking. See [this document](https://bjwb.seiee.sjtu.edu.cn/bkjwb/info/14274.htm) on my school's website. In the attached list you can find my name at the very top of the column for my department[^1]<sup>,</sup>[^2].

[^1]: The online document and its attachment are both in Chinese.
[^2]: For your information, as of this writing, [Internet Archive](https://web.archive.org/) has taken several snapshots of both, in case the original documents become not accessible someday somehow.

*The final ranking*, depending on the algorithm being used, ranges from **6** to **11/128**. These numbers *can* be certified[^3] but for obvious reasons I'm less willing to put them in my profiles. They are different, as mentioned above, because (1) they are based on all eight semesters (2) they do not take the bonus score into account.

[^3]: In fact, I don't know these numbers until three years after my graduation. *The moment I learned about them, I decided to write this whole thing you are now reading*.

**The sole purpose of this note, is to give some more context about the ranking I ever presented, and to defend the integrity of myself, which, has always been valued with first priority.**

If you have further questions, please contact me.

<p style="text-align: right">
  Wentao &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>
  2022-10-21
</p>
