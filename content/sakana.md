---
title: "Sakana~ 🐟"
---

Acknowledgement:

* [Lycoris Recoil](https://lycoris-recoil.com/)
* [大伏アオ](https://twitter.com/blue00f4)
* [itorr/sakana](https://github.com/itorr/sakana)

<div class="sakana-box" style="height: 600px"></div>

<script src="https://cdn.jsdelivr.net/npm/sakana"></script>

<script>
Sakana.init({
    el:                 '.sakana-box',     // 启动元素 node 或 选择器
    canSwitchCharacter:   false,           // 允许换角色
    inertia:              0.01,            // 惯性
});
Sakana.setMute(false);
</script>
