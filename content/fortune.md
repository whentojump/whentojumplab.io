---
title: "Fortune Cookies 🍪"
---

<br>

```shell
$ fortune --help
Random excerpts I would like to share. Updated inactively. Sample:

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer finibus pretium
placerat. Nunc sit amet.

                -- whentojump
```

<br>

A [comment](https://softwareengineering.stackexchange.com/questions/373269/what-does-the-emulation-do-in-the-linker#comment932238_373269) by _Useless_ @ Stack Overflow, emphasis either theirs or mine:

> ..._Idle curiosity_ isn't a great way of learning about complex systems, because there are too many unmotivated "but why" questions to make progress, and they don't produce anything but a sequence of disconnected facts. _Motivated curiosity_, when you're trying to achieve something specific and know what that is, is generally more productive. And a comment that "this is a sane default and you don't normally need to change it" is useful information in itself.

<br>

Literature v.s. real world

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Given today’s leak of the NovelAI models, I find it pretty funny that after all the academic work on elaborate “model stealing” attacks, real world model stealing is more like “someone put our model weights on a torrent site”</p>&mdash; Brendan Dolan-Gavitt (@moyix) <a href="https://twitter.com/moyix/status/1578180849933090821?ref_src=twsrc%5Etfw">October 7, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
