---
title: "Résumé"
---

<div style="color: var(--warning)">
  <p>Recently this résumé has been less actively maintained. But let me try to keep it sync'ed :)</p>
  <p style="text-align: right">Wentao<br>May 2024</p>
</div>

[Wentao](https://wentao.systems/)'s résumé

- Based on the great [template](https://github.com/sb2nov/resume) by sb2nov (licensed under MIT)
- Presented to you with [GitLab CI](https://docs.gitlab.com/ee/ci/)

<a class="gitlab-badge" href="https://gitlab.com/whentojump/resume/-/commits/wtj-public"><img alt="last modified" src="https://img.shields.io/gitlab/last-commit/whentojump/resume?label=modified&color=blue" /></a> <a class="gitlab-badge" href="https://gitlab.com/whentojump/resume/-/pipelines"><img alt="pipeline status" src="https://img.shields.io/gitlab/pipeline-status/whentojump/resume?branch=wtj-public" /></a> <a class="gitlab-badge" href="https://gitlab.com/whentojump/resume/-/releases"><img alt="latest release" src="https://img.shields.io/gitlab/v/release/whentojump/resume?color=pink" /></a>

<!--
  Use inline HTML with "gitlab-badge" class so that I can reuse this README.md somewhere else, e.g. static site frameworks. The class is a trick for adjusting the style if the badge is not rendered as expected by default.
-->

## View the latest commit online or download it

[View PDF online](https://gitlab.com/whentojump/resume/-/jobs/artifacts/wtj-public/file/wentao-resume.pdf?job=build)

[Download PDF](https://gitlab.com/whentojump/resume/-/jobs/artifacts/wtj-public/raw/wentao-resume.pdf?job=build)

## Build from source

### 0. Common options and targets

Clean:

```shell
make clean
```

Be verbose:

```shell
make V=1
```

**TODO (May 2024)**
Let me actually rework the build system and switch to [`texlive/texlive`](https://hub.docker.com/r/texlive/texlive).

<!-- Need to be reworked { -->

### 1. Without `latexmk` installed

Optionally, pull the Docker image in a separate step (privilege needed):

```shell
docker pull blang/latex:latest
```

Build (privilege needed):

```shell
make
```

### 2. With `latexmk` installed (say, inside a container during CI)

```shell
make INSIDE_CONTAINER=1
```

<!-- } // Need to be reworked -->

## Boring fact

You may have spotted, there is a version notice at the footnote, which reads something like:

> Revised in March 2022 (`a694d61`). Please check https://wentao.systems/resume/ for the online version with possible updates.

The ~~funny~~boring part is, this is generated at compile time, and the placeholders in the original document are [January 1970](https://en.wikipedia.org/wiki/Unix_time) and [`deadbeef`](https://en.wikipedia.org/wiki/Magic_number_(programming)), respectively. Well, sounds like bad jokes told by some computer weirdos...

In rare cases, that is, when you _really_ want to include the two phrases in other parts of the text, the joke will then become a concern. The author just won't take care of that and you may work around it by improving the [`Makefile`](https://gitlab.com/whentojump/resume/-/blob/wtj-public/Makefile) :)

<!-- Use absolute link, again, to be portable -->
